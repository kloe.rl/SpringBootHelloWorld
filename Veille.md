## Qu'est-ce que Spring ?

Spring est un framework, il permet d’accélérer le développement d’applications d’entreprise (notamment le développement d’applications Web et d’API Web). **Spring Framework** avait pour objectif de proposer une alternative au modèle d’architecture logiciel proposé par la plate-forme J2EE.

L’idée centrale du **Spring Framework** est de n’imposer aucune norme de développement
ni aucune contrainte technique sur la façon dont les développeurs doivent coder
leurs applications.

Le **Spring Framework** propose de bâtir des applications qui embarquent
elles-mêmes les services dont elles ont besoin : c’est pour cette raison, que
l’on qualifie parfois le Spring Framework de *conteneur léger*.

## Quelle est la différence entre Spring et Spring Boot ?

Le projet **Spring Boot** est une extension du Spring Framework pour mettre en place
rapidement des applications Java. Grâce à un système modulaire de dépendances
et un principe de configuration automatique, il permet de disposer d’une
structure de projet complète et immédiatement opérationnelle.

Spring Boot n’est pas un nouvelle version du Spring Framework. Il s’agit d’une utilisation particulière du *framework*, évitant aux développeurs de gérer une complexité technique souvent inutile au moment de l’initialisation d’un projet.

## Qu'est-ce que l'inversion de contrôle ?

L’inversion de contrôle (*Inversion of control* ou IoC) est un principe d’architecture conduisant à inverser le flux de contrôle par rapport au développement traditionnel.

Dans une approche **IoC**, le flux de contrôle est orienté du code tiers vers le code de mon application. Le code que je fournis sera **sous le contrôle du code tiers**.

Pour pouvoir être mise en pratique, l’inversion de contrôle implique l’existence d’un composant supplémentaire. 

La construction des objets de notre application va être déléguée à ce composant
que l’on appelle un conteneur IoC (*IoC container*). Ce conteneur accueille un
ensemble d’objets dont il a la responsabilité de gérer le cycle de vie.

Dans la terminologie du Spring Framework, le conteneur IoC est constitué d’un ou de plusieurs **contextes d’application**.

## Qu'est-ce que l'injection de dépendance ?

L’**injection de dépendance** est un mécanisme simple à mettre en œuvre dans le cadre de la programmation objet et qui permet de diminuer le couplage entre deux ou plusieurs objets.

On peut faire de l’injection de dépendance par le **constructeur** ou par **setter**.